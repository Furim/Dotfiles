#!/bin/bash
# This script takes the tag information from DWL from stdin.
swaybg -m fill -i /home/furimmm/wallpaper.jpg &
#************************************CONFIG*******************************************************#
#Names in the order dtao sees them, check the index with dtao -xs <display no.>
displays=("DP-1")

# colors are formated RRGGBB
active_color="#689d6a"
inactive_color="#282828"
text_color="#ebdbb2"

font="monospace:size=14"

# Width of the tray area in characters
traywidth=50

#How many characters to pad before the tray, as dtao does not yet have movement commands.
#This depends on your displays, so play around with the values until everything fits just right
declare -A pad=([DP-1]=20)


# The stdout of this funciton is displayed in the tray area on the right of the bar, one line at a time.
# Lines are formated "DISPLAY_NAME tray <stuff to display>"
# You can have different trays per display, or all the same like it is here.
# Make sure the line is output all in one command, not in chunks, or things will break.
tray(){
        while true; do
                for d in ${displays[@]}; do



                        output="$d tray `date`"
                        output="$output"

                        echo $output
                         

                done

                # How often the tray area updates
                sleep 5
        done
}




#************************************BORING SCRIPT STUFF******************************************#

# This function spawns the dtao instances for each display.
bar(){
        cat -- | sed -n -u "/^$1/p" | stdbuf -oL cut -b 3- |\
        dtao -z -z -L b -ta l -bg "$inactive_color" -fg "$text_color" -fn "$font" -xs $1 \
         
}


declare -A title
declare -A taginfo
declare -A isactive
declare -A layout
declare -A dno
declare -A traystr

#Initialize some variables
i=1
barcmd="tee "
for d in ${displays[@]}; do
        taginfo[$d]="0 1"
        isactive[$d]="0"
        layout[$d]="[]="
        dno[$d]=$i
        barcmd="$barcmd >(bar $i) "
        i=$((i+1))
done

# Main loop
( tray & cat -- ) | while IFS='\n' read -r line; do

        mon=`echo $line | cut -d ' ' -f 1`
        dtype=`echo $line | cut -d ' ' -f 2`


        case "$dtype" in
                title)
                        if [ "${title[$mon]}" == "$(echo $line | cut -s -d ' ' -f 3- | tr -dc '[[:print]]')" ]
                        then
                                continue
                        fi
                        title[$mon]=`echo $line | cut -d ' ' -f 3- | tr -dc '[[:print:]]'`
                        ;;

                tags)
                        if [ "${taginfo[$mon]}" == "$(echo $line | cut -d ' ' -f 3-)" ]
                        then
                                continue
                        fi
                        taginfo[$mon]=`echo $line | cut -d ' ' -f  3-`
                        ;;

                selmon)
                        if [ "${isactive[$mon]}" == "$(echo $line | cut -d ' ' -f  3)" ]
                        then
                                continue
                        fi
                        isactive[$mon]=`echo $line | cut -d ' ' -f 3`
                        ;;

                layout)
                        if [ "${layout[$mon]}" == "$(echo $line | cut -d ' ' -f 3-)" ]
                        then
                                continue
                        fi
                        layout[$mon]=`echo $line | cut -d ' ' -f 3-`
                        ;;
                tray)
                        traystr[$mon]=`echo $line | cut -d ' ' -f 3-`
                        ;;
                *)
                        continue
        esac

    ctags=`echo "${taginfo[$mon]}" | cut -d ' ' -f 1`

    mtags=`echo "${taginfo[$mon]}" | cut -d ' ' -f 2`

    outstring=""

        if [ "${isactive[$mon]}" == "1" ]        

        then
                titlecolor="$active_color"
        else
                titlecolor="$inactive_color"
        fi

    for i in {0..8};
    do

        mask=$((1<<i))
        if (( "${ctags}" & $mask ));
        then
            n="*$((i+1))"
        else
            n=" $((i+1))"
        fi
        if (( "${mtags}" & $mask ));
        then
                        outstring="${outstring}^bg($active_color)$n "
        else
                        outstring="${outstring}^bg($inactive_color)$n "
        fi
    done
        outstring="${dno[$mon]} $outstring^bg() ${layout[$mon]} ^bg($titlecolor) ${title[$mon]}"

        printf "%-${pad[$mon]}s%s%${traywidth}s\n" "$outstring" "^bg()" "${traystr[$mon]}"

done | eval "$barcmd" >> /home/furimmm/dtao/dtao