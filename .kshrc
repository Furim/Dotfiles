neofetch


alias serverinfo="pactl info | grep "Server Name""
alias dwl="dwl -s /home/furimmm/waybar_active.sh"


alias grep="/usr/bin/grep $GREP_OPTIONS"
alias img="mpv --pause"


unset GREP_OPTIONS


alias ls="ls --color=auto"

alias zip-tar="tar -cf" # typeherename.tar.gz le_folder/
alias encrypt="gpg -c --no-symkey-cache --cipher-algo AES256"

# This file contains examples of some of the things you may want to
# include in a user startup file.

# handle bash/zsh SHLVL variable

# skip this setup for non-interactive shells
[[ -o interactive && -t 0 ]] || return

# disable core dumps
ulimit -c 0

# Environment variables. These could go in .profile if you prefer
export VISUAL=vi
export EDITOR=$VISUAL
export PAGER=less
export GZIP=-9

# set some shell options
set -o emacs
#  -o trackall -o globstar

# specify search path for autoloadable functions
FPATH=/usr/share/ksh/functions:~/.func

# avoid certain file types in completion
FIGNORE='@(*.o|~*)'

# save more commands in history
HISTSIZE=500
HISTEDIT=$EDITOR


# avoid problems with long argument lists for some commands (like xargs)
alias cp='command -x cp'  mv='command -x mv'  grep='command -x grep'

# some short functions
empty() { echo $'\e[3J'; }
mere() { nroff -man -Tman $1 | ${MANPAGER:-less}; }
setenv() { export "${1}${2:+=$2}"; }

# Use keyboard trap to map keys to other keys
# note that escape sequences vary for different terminals so these
# may not work for you
trap '.sh.edchar=${keymap[${.sh.edchar}]:-${.sh.edchar}}' KEYBD
keymap=(
  [$'\eOD']=$'\eb'   # Ctrl-Left  -> move word left
  [$'\eOC']=$'\ef'   # Ctrl-Right -> move word right
  [$'\e[3~']=$'\cd'  # Delete     -> delete to right
  [$'\e[1~']=$'\ca'  # Home       -> move to beginning of line
  [$'\e[4~']=$'\ce'  # End        -> move to end of line
)




alias __A=`echo "\020"`     # up arrow = ^p = back a command
alias __B=`echo "\016"`     # down arrow = ^n = down a command
alias __C=`echo "\006"`     # right arrow = ^f = forward a character
alias __D=`echo "\002"`     # left arrow = ^b = back a character
alias __H=`echo "\001"`     # home = ^a = start of line
alias __Y=`echo "\005"`     # end = ^e = end of line



keybd_trap () {
  case ${.sh.edchar} in
    $'\f')    .sh.edchar=$'\e\f';;  # clear-screen (THIS QUESTION)
    $'\e[1~') .sh.edchar=$'\001';;  # Home = beginning-of-line
    $'\e[4~') .sh.edchar=$'\005';;  # End = end-of-line
    $'\e[5~') .sh.edchar=$'\e>';;   # PgUp = history-previous
    $'\e[6~') .sh.edchar=$'\e<';;   # PgDn = history-next
    $'\e[3~') .sh.edchar=$'\004';;  # Delete = delete-char
  esac
}
trap keybd_trap KEYBD


# keep a shortened version of the current directory for the prompt

# put the current directory and history number in the prompt
export PS1=$'\E[1;32m[$(logname)@$(hostname -s):$PWD $] ~ \E[0m'